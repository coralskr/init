# sh -c "$(wget https://gitee.com/coralskr/init/raw/master/manjaro-init-test.sh -O -)"
# https://www.raspberrypi.org/documentation/computers/config_txt.html
set -eux

setup_source_mirror() {
	sudo timedatectl set-local-rtc 0
	sudo systemctl enable --now systemd-timesyncd

	cat > ./sources.list << "EOF"
    Server = https://mirrors.huaweicloud.com/manjaro/arm-stable/$repo/$arch
    Server = https://mirrors.ustc.edu.cn/manjaro/arm-stable/$repo/$arch
    Server = https://mirrors.tuna.tsinghua.edu.cn/manjaro/arm-stable/$repo/$arch
    Server = https://mirrors.tuna.tsinghua.edu.cn/manjaro/arm-stable/$repo/$arch
EOF
    sudo cp ./sources.list /etc/pacman.d/mirrorlist
    rm ./sources.list

    cat >> ./pacman.conf << "EOF"
    [options]
    HoldPkg      = pacman glibc manjaro-system
    SyncFirst    = manjaro-system manjaro-keyring manjaro-arm-keyring archlinux-keyring archlinuxarm-keyring
    Architecture = aarch64
    CheckSpace
    ParallelDownloads = 5
    SigLevel = TrustAll
    LocalFileSigLevel = Optional
    [core]
    Include = /etc/pacman.d/mirrorlist
    [extra]
    Include = /etc/pacman.d/mirrorlist
    [community]
    Include = /etc/pacman.d/mirrorlist
    [archlinuxcn]
    Server = https://mirrors.tuna.tsinghua.edu.cn/archlinuxcn/$arch
EOF
    sudo cp ./pacman.conf /etc/pacman.conf
    rm ./pacman.conf
}

install_essential() {
    echo "begin install essential"
    sudo pacman -Syyu
    sudo pacman -S yay
    sudo pacman -S vim 
    sudo pacman -S zsh
    sudo pacman -S archlinuxcn-keyring
}

install_uurufa() {
    yay -S fcitx-im
    yay -S fcitx-configtool
    yay -S fcitx-sogoupinyin
    cat > ~/.xprofile << "EOF"
    gsettings set org.gnome.settings-daemon.plugins.xsettings overrides "{'Gtk/IMModule':<'fcitx'>}"
EOF
}

zsh_start() {
    sh -c "$(wget https://gitee.com/kuaibiancheng/kbc_setup/raw/master/install.ohmyz.sh -O -)"
    sudo chsh -s /bin/zsh
}

vnc_install() {
    sudo pacman -S base-devel
    git clone https://gitee.com/AAlan/realvnc-manjaro.git
    cd realvnc-manjaro
    makepkg -si
    sudo systemctl enable vncserver-x11-serviced.service
    sudo systemctl start vncserver-x11-serviced.service
    sudo systemctl status vncserver-x11-serviced.service
    sudo pacman -S net-tools
}

__main() {
    setup_source_mirror
    install_essential
    install_uurufa
    zsh_start
	vnc_install
}
 
__main

# fcitx-configtool
