# 常见问题记录

### x509: certificate has expired or is not yet valid

可能是系统时间没同步导致

可以用下面的方式来处理(时间使用当前时间即可)
```
date -s "2021-09-17 00:00:00"
```

### 没鼠标怎么办

淘宝


### /boot/config.txt 改坏了怎么办

这里有一份备份的默认配置, 可以试试

```
gpu_mem=64
initramfs initramfs-linux.img followkernel
kernel=kernel8.img
arm_64bit=1
disable_overscan=1

#enable sound
dtparam=audio=on
hdmi_drive=2
hdmi_safe=1

#enable vc4
dtoverlay=vc4-fkms-v3d
max_framebuffers=2
disable_splash=1
```

### 不插显示器，直接开机使用vnc连接显示 cannot currently show the desktop

如果 /boot/config.txt改过，那说明/boot/config.txt改炸了

如果没改过，说明应该改，请使用上面的默认配置

关键设置: hdmi_safe=1

想要更高的分辨率，请自己摸索配置

连接在这里

https://www.raspberrypi.org/documentation/computers/config_txt.html

### vnc连不上怎么办

确定指定ip能ping通

如果能ping通的话，试试能不能ssh登录上去

如果能ssh上去，那你tm的根本没装vnc

请安装 vnc (脚本里有，但是不是自动安装的)

如果还不行，请检查 

sudo systemctl status vncserver-x11-serviced.service

看下是不是 runing 状态

```
 Active: active (running) since Fri 2021-09-17 23:25:05 CST; 25min ago
```

### 如何安装 pip

wget https://gitee.com/AAlan/realvnc-manjaro/raw/master/install_pip.sh && chmod +x install_pip.sh && ./install_pip.sh

### 手动换源

sudo pacman-mirrors -i -c China -m rank
